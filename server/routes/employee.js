const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

router.get('/faculty/:ClassroomId', (request, response) => {
    const {ClassroomId} = request.params
  
    const statement = `select f.FirstName,f.LastName,f.Address,f.Contact,f.Age,f.Experience from Faculty f inner join Classroom c  
                       on f.Id = c.FacultyId where c.Id = ${ClassroomId}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))})
  
  })
router.get('/classroom/:ClassroomId', (request, response) => {
    const {ClassroomId} = request.params
  
    const statement = `select * from Classroom where Id = ${ClassroomId}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))})
  
  })  
  


// ---------------------------------------
//                  POST
// ---------------------------------------


router.post('/signin', (request, response) => {
    const {email, password} = request.body
    const statement = `select * from employee where email = '${email}' and password = '${password}'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
  })


// ---------------------------------------
//                  PUT
// ---------------------------------------

router.put('/editProfile/:empId', (request, response) => {
    const {empName, email, mobile} = request.body
    const { empId } = request.params
    const statement = `update employee set empName = '${empName}', email = '${email}', mobile = '${mobile}' where empId = ${empId}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))})
  
  })

module.exports = router