const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
// routers
const routeEmployee = require('./routes/employee')
const routeAdmin = require('./routes/admin')

const app = express()
app.use(cors('*'))
app.use(bodyParser.json())



// add the routes
app.use('/employee', routeEmployee)
app.use('/admin', routeAdmin)

// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})