import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  url = 'http://localhost:3000/employee'

  constructor(
    private router: Router,
    private httpClient: HttpClient) { }

  login(email: string, password: string) {
    const body = {
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/signin', body)
  }

  edit(id,name, email, phone) {
    const body = {
      email: email,
      phone: phone,
      empName: name
    }

    return this.httpClient.put(this.url + '/editProfile/' + id, body)
  }

  getmeetings(){
    return this.httpClient.get(this.url + '/meetings')
  }
  getqueries(id){
    return this.httpClient.get(this.url + '/queries/' + id)
  }
  getleaves(id){
    return this.httpClient.get(this.url + '/leaves/' + id)
  }
  getemployee(id){
    return this.httpClient.get(this.url + '/' + id)
  }
}
