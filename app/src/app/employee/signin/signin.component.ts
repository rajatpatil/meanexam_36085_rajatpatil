import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  email = ''
  password = ''

  constructor(
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
  }

  onLogin() {
      this.employeeService
        .login(this.email, this.password)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const data = response['data']
            console.log(data)
            alert(`Welcome ${data[0]['empName']} `)
             sessionStorage['empId'] = data[0]['empId']
            // goto the dashboard
            this.router.navigate(['/home/employee/home'])

          } else {
            alert(response['error'])
          }
        })
    }
  

}
