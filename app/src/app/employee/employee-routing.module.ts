import { ApplyLeaveComponent } from './apply-leave/apply-leave.component';
import { HomeComponent } from './../employee/home/home.component';
import { SigninComponent } from './signin/signin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { QueriesComponent } from './queries/queries.component';

const routes: Routes = [
  { path: 'login', component: SigninComponent},
  { path: 'home', component: HomeComponent},
  { path: 'edit', component: EditProfileComponent},
  { path: 'apply', component: ApplyLeaveComponent},
  { path: 'meetings', component: MeetingsComponent},
  { path: 'queries', component: QueriesComponent},
  { path: 'leaves', component: ApplyLeaveComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
