import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { SigninComponent } from './signin/signin.component';
import { EmployeeService } from './employee.service';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ApplyLeaveComponent } from './apply-leave/apply-leave.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { QueriesComponent } from './queries/queries.component';


@NgModule({
  declarations: [SigninComponent, HomeComponent, EditProfileComponent, ApplyLeaveComponent, MeetingsComponent, QueriesComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers:[EmployeeService]
})
export class EmployeeModule { }
