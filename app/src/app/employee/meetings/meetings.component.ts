import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.css']
})
export class MeetingsComponent implements OnInit {
  meetings = []

  constructor(
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.loadMeetings()
  }

  loadMeetings() {
    this.employeeService
      .getmeetings()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.meetings = response['data']
          console.log(this.meetings)
        } else {
          console.log(response['error'])
        }
      })
  }
}
