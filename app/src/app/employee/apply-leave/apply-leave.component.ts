import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-apply-leave',
  templateUrl: './apply-leave.component.html',
  styleUrls: ['./apply-leave.component.css']
})
export class ApplyLeaveComponent implements OnInit {
  leaves = []
  employee = {}
  constructor(
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
   this.loademployee()
    this.loadLeaves()
  }

  loadLeaves() {
    this.employeeService
      .getleaves(sessionStorage['empId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.leaves = response['data']
          console.log(this.leaves)
        } else {
          console.log(response['error'])
        }
      })
  }
  loademployee() {
    this.employeeService
      .getemployee(sessionStorage['empId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.employee = response['data'][0]
          console.log(this.employee)
        } else {
          console.log(response['error'])
        }
      })
  }
}
