import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-queries',
  templateUrl: './queries.component.html',
  styleUrls: ['./queries.component.css']
})
export class QueriesComponent implements OnInit {
  queries = []

  constructor(
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.loadQueries()
  }

  loadQueries() {
    this.employeeService
      .getqueries(sessionStorage['empId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.queries = response['data']
          console.log(this.queries)
        } else {
          console.log(response['error'])
        }
      })
  }

}
