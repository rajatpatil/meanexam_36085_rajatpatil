import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  email = ''
	phone= ''
	name= ''

  constructor(
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
  }

  onUpload() {
    if (this.name.length == 0 || this.email.length == 0 || this.phone.length == 0) {
      alert('please enter all details')
    } else {
      this.employeeService
        .edit(sessionStorage['empId'],this.name,this.email,this.phone)
        .subscribe(response => {
          if (response['status'] == 'success') {
             alert(`Edit success`)
            this.router.navigate(['/home/employee/home'])

          } else {
            console.log(response['error'])
          }
        })
    }
  }
}
