import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo:'/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { 
    path: 'home',
    component: HomeComponent, 
    children: [
      { path: 'employee', loadChildren: () => import('./employee/employee.module').then(m => m.EmployeeModule ) },
      { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule ) },
]
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
